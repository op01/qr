# QR #

A QR reader app implemented using [QR Scanner library](https://github.com/nimiq/qr-scanner).

The application is available at [qar.netlify.app](https://qar.netlify.app).
