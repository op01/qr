import QrScanner from 'https://cdn.jsdelivr.net/npm/qr-scanner@1/qr-scanner.min.js'

const workerFile = 'https://cdn.jsdelivr.net/npm/qr-scanner@1/qr-scanner-worker.min.js'
const workerBlob = new Blob([`importScripts('${workerFile}');`], { "type": 'application/javascript' })
const workerUrl = URL.createObjectURL(workerBlob)
QrScanner.WORKER_PATH = workerUrl

const resultEl = document.getElementById("result")
const imagePickerEl = document.getElementById("imagePicker")
const imageInputAreaEL = document.getElementById('imageInputArea')

function setResult(text){
  resultEl.innerText = text
}

// Drag & Drop

imageInputAreaEL.addEventListener('dragover',e=>{
  e.preventDefault()
})

imageInputAreaEL.addEventListener('drop',e=>{
  e.preventDefault()
  for(let dti of e.dataTransfer.items){
    console.log(dti)
    if(dti.kind === 'file') {
      const file = dti.getAsFile()
      scanQR(file)
    }
  }
})

// File picker

imagePickerEl.addEventListener('change', e => {
  for(let file of imagePickerEl.files){
    scanQR(file)
  }
})


// Clipboard

async function readClipboard() {
  try {
    const clipboardItems = await navigator.clipboard.read()
    for (const clipboardItem of clipboardItems) {
      for (const type of clipboardItem.types) {
        const blob = await clipboardItem.getType(type)
        const file = new File([blob], "name", { type: "image/jpeg", })
        scanQR(file)
      }
    }
  } catch (err) {
    console.error(err.name, err.message);
  }
}

document.addEventListener('paste', e => {
  e.preventDefault()
  readClipboard()
})

async function scanQR(file){
  try {
    const text = await QrScanner.scanImage(file)
    console.log(text)
    setResult(text)
  } catch (error) {
    setResult(error.toString())
  }
}

console.log('Hello')